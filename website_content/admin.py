from django.contrib import admin
from django.db import models
from translations.admin import TranslatableAdmin, TranslationInline
from markdownx.widgets import AdminMarkdownxWidget
from django_google_maps import widgets as map_widgets
from django_google_maps import fields as map_fields

from website_content.models import Article, AboutUs, Event


class ArticleAdmin(TranslatableAdmin):
    inlines = [TranslationInline, ]
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }


class AboutUsAdmin(TranslatableAdmin):
    inlines = [TranslationInline, ]
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }


class EventAdmin(TranslatableAdmin):
    inlines = [TranslationInline, ]
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
        map_fields.AddressField: {'widget': map_widgets.GoogleMapsAddressWidget},
    }


admin.site.register(Article, ArticleAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Event, EventAdmin)
