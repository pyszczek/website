from django.conf.urls import include
from django.urls import reverse_lazy, path
from django.views.generic import RedirectView
from markdownx import urls as markdownx

from . import views

urlpatterns = [
    path('', RedirectView.as_view(url=reverse_lazy('index'))),
    path("graphql", views.PrivateGraphQLView.as_view(graphiql=True)),
    path('aboutus/', views.IndexListView.as_view(), name='aboutus'),
    path('index/', views.ArticleListView.as_view(), name='index'),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('events/', views.EventView.as_view(), name='events'),
    path('lang/<lang>/', views.set_page_language, name='language'),
    path('markdownx/', include(markdownx)),
]
