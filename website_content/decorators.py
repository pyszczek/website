from django.utils import translation


def check_for_language_cookie(function):
    def wrap(request, *args, **kwargs):
        if 'LANGUAGE_SESSION_KEY' not in request.session:
            user_language = translation.get_supported_language_variant(translation.get_language_from_request(request))
            translation.activate(user_language)
            request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        return function(request, *args, **kwargs)
    return wrap
