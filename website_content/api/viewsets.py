from rest_framework import viewsets
from django.utils import translation
from django.utils.decorators import method_decorator

from website_content.api.serializers import ArticleSerializer, EventSerializer, AboutUsSerializer
from website_content.models import Article, Event, AboutUs
from website_content.decorators import check_for_language_cookie


@method_decorator(check_for_language_cookie, name='dispatch')
class ArticleViewSet(viewsets.ModelViewSet):
    serializer_class = ArticleSerializer

    def get_queryset(self):
        session = self.request.session
        return Article.objects.all().order_by('-created_date').translate(session[translation.LANGUAGE_SESSION_KEY])


@method_decorator(check_for_language_cookie, name='dispatch')
class EventViewSet(viewsets.ModelViewSet):
    serializer_class = EventSerializer

    def get_queryset(self):
        session = self.request.session
        return Event.objects.all().order_by('-event_date').translate(session[translation.LANGUAGE_SESSION_KEY])


@method_decorator(check_for_language_cookie, name='dispatch')
class AboutUsViewSet(viewsets.ModelViewSet):
    serializer_class = AboutUsSerializer

    def get_queryset(self):
        session = self.request.session
        return AboutUs.objects.all().translate(session[translation.LANGUAGE_SESSION_KEY])
