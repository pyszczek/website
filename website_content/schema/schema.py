from graphene import relay, ObjectType
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from website_content.models import Article, User, Event


class ArticleNode(DjangoObjectType):
    class Meta:
        model = Article
        filter_fields = {
            'title': ['exact', 'icontains', 'istartswith'],
            'body': ['exact', 'icontains'],
            'author': ['exact'],
        }
        interfaces = (relay.Node,)


class UserNode(DjangoObjectType):
    class Meta:
        model = User
        exclude_fields = ('password', 'username',)
        filter_fields = {
            'first_name': ['exact', 'icontains', 'istartswith'],
            'last_name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


class EventNode(DjangoObjectType):
    class Meta:
        model = Event
        filter_fields = {
            'title': ['exact', 'icontains', 'istartswith'],
            'description': ['exact', 'icontains'],
            'event_date': ['exact', 'icontains', 'istartswith'],
            'address': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


class Query(ObjectType):
    all_articles = DjangoFilterConnectionField(ArticleNode)
    all_users = DjangoFilterConnectionField(UserNode)
    all_events = DjangoFilterConnectionField(EventNode)
    article = relay.Node.Field(ArticleNode)
    user = relay.Node.Field(UserNode)
    event = relay.Node.Field(EventNode)
