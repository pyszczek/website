from django.db.models.signals import post_save
from django.dispatch import receiver
from website_content.models import Article
from website_content.slack_bot import slackbot


@receiver(post_save, sender=Article)
def send_message(sender, instance, **kwargs):
    """Listens to a new Article created event and sends a message to Slack."""

    if instance.bot_post:
        slackbot.message_as_bot(instance)
