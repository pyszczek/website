from rest_framework import viewsets
from django.utils import translation
from django.utils.decorators import method_decorator

from users.api.serializers import ProfileSerializer
from users.models import Profile
from website_content.decorators import check_for_language_cookie


@method_decorator(check_for_language_cookie, name='dispatch')
class ProfileViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        session = self.request.session
        return Profile.objects.all().order_by('-user_id').translate(session[translation.LANGUAGE_SESSION_KEY])
