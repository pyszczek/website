from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _

from website_content.models import Event


class RegisterAsSpeakerForm(UserCreationForm):
    next_event = forms.ModelChoiceField(queryset=Event.objects.all(), label=_('Which event do you want to speak on:'))
    presentation_title = forms.CharField(max_length=256, label=_('Your presentation title:'))

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'next_event',
            'presentation_title')

    def register_user(self):
        user = self.save()
        user.refresh_from_db()
        user.profile.next_event = self.cleaned_data.get('next_event')
        user.profile.presentation_title = self.cleaned_data.get('presentation_title')
        user.save()
        raw_password = self.cleaned_data.get('password1')
        user = authenticate(username=user.username, password=raw_password)
        return user


class UpdatePresentationDetailsForm(forms.Form):
    next_event = forms.ModelChoiceField(queryset=Event.objects.all(), label=_('Which event do you want to speak on:'))
    presentation_title = forms.CharField(max_length=256, label=_('Your presentation title:'))

    def update_details(self, caller):
        user = User.objects.get(id=caller.id)
        user.profile.next_event = self.cleaned_data.get('next_event')
        user.profile.presentation_title = self.cleaned_data.get('presentation_title')
        user.save()


class ChangeEmailForm(forms.Form):
    new_email = forms.EmailField(max_length=256, label=_('New e-mail:'))
