from graphene import relay, ObjectType
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from users.models import Profile


class ProfileNode(DjangoObjectType):
    class Meta:
        model = Profile
        filter_fields = {
            'description': ['exact', 'icontains'],
            'user': ['exact'],
        }
        interfaces = (relay.Node,)


class Query(ObjectType):
    all_profiles = DjangoFilterConnectionField(ProfileNode)
    profile = relay.Node.Field(ProfileNode)
