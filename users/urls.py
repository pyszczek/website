from django.contrib.auth import views as auth_views
from django.urls import path

from . import views


urlpatterns = [
    path('register/', views.RegisterSpeakerView.as_view(), name='register_speaker'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('change-email/', views.ChangeEmailView.as_view(), name='change_email'),
    path('change-password/', views.PasswordChangeMessageView.as_view(), name='change_password'),
    path('reset-password/', views.PasswordResetMessageView.as_view(), name='reset_password'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmMessageView.as_view(), name='password_reset_confirm'),
]
