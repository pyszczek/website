from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.views import PasswordChangeView, PasswordResetView, PasswordResetConfirmView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic.edit import FormView

from website_content.decorators import check_for_language_cookie
from users.forms.forms import RegisterAsSpeakerForm, UpdatePresentationDetailsForm, ChangeEmailForm


@method_decorator(check_for_language_cookie, name='dispatch')
class RegisterSpeakerView(FormView):
    """Serves both register a new user form and edit presentation details form
    depending on if the user is logged in or not."""

    template_name = 'speaker_registration.html'
    success_url = '/index'

    def get_form_class(self):
        if self.request.user.is_authenticated:
            return UpdatePresentationDetailsForm
        else:
            return RegisterAsSpeakerForm

    def form_valid(self, form):
        caller = self.request.user
        if caller.is_authenticated:
            form.update_details(caller)
            messages.success(self.request, _('You have successfully changed your presentation details!'))
        else:
            user = form.register_user()
            login(self.request, user)
            messages.success(self.request, _('You have successfully created your speaker profile!'))
        return super().form_valid(form)


@method_decorator(check_for_language_cookie, name='dispatch')
class ChangeEmailView(LoginRequiredMixin, FormView):
    form_class = ChangeEmailForm
    template_name = 'change_email.html'
    success_url = '/index'

    def form_valid(self, form):
        user = self.request.user
        user.email = form.cleaned_data.get('new_email')
        user.save()
        messages.success(self.request, _('You have successfully changed your e-mail!'))
        return super().form_valid(form)


@method_decorator(check_for_language_cookie, name='dispatch')
class PasswordChangeMessageView(SuccessMessageMixin, PasswordChangeView):
    template_name = 'change_password.html'
    success_url = '/index'
    success_message = _('You have successfully changed your password!')


@method_decorator(check_for_language_cookie, name='dispatch')
class PasswordResetMessageView(SuccessMessageMixin, PasswordResetView):
    template_name = 'reset_password.html'
    success_url = '/index'
    success_message = _('E-mail with password reset instructions was sent successfully')


@method_decorator(check_for_language_cookie, name='dispatch')
class PasswordResetConfirmMessageView(SuccessMessageMixin, PasswordResetConfirmView):
    template_name = 'confirm_reset.html'
    success_url = '/index'
    success_message = _('You have successfully reset your password!')


def logout_user(request):
    logout(request)
    messages.success(request, _('You were logged out successfully!'))
    return redirect('/index')
