from django.contrib.auth.models import User
from django.db import models
from translations.models import Translatable
from markdownx.models import MarkdownxField
from django.db.models.signals import post_save
from django.dispatch import receiver
from website_content.models import Event


class Profile(Translatable):
    description = MarkdownxField(blank=True)
    photo = models.URLField(blank=True)
    next_event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True)
    presentation_title = models.CharField(max_length=256, blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

    class Meta:
        ordering = ['id']

    class TranslatableMeta:
        fields = ['description']

    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
        instance.profile.save()
