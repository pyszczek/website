from django.contrib import admin
from django.db import models
from translations.admin import TranslatableAdmin, TranslationInline
from markdownx.widgets import AdminMarkdownxWidget

from users.models import Profile


class ProfileAdmin(TranslatableAdmin):
    inlines = [TranslationInline, ]
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }


admin.site.register(Profile, ProfileAdmin)
