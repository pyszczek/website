const navAboutUs = document.querySelector('.nav-aboutus');
navAboutUs.setAttribute('aria-selected', true);

const tabs = document.querySelectorAll('.about-us__photo-item');
const firstTab = document.querySelector('.about-us__photo-item');
firstTab.setAttribute('aria-expanded', true);

const expandTab = event => {
  for (let tab of tabs) {
    tab.setAttribute('aria-expanded', false);
  }
  event.target.setAttribute('aria-expanded', true);
}

const hideTab = event => event.target.setAttribute('aria-expanded', false);

for (let tab of tabs) {
  tab.addEventListener('mouseenter', expandTab);
  // tab.addEventListener('mouseleave', hideTab);
}
