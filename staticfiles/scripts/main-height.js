const main = document.querySelector('main');

const setHeight = () => {
  const navHeight = document.querySelector('nav').offsetHeight;
  const footerHeight = document.querySelector('footer').offsetHeight;
  const viewHeight = document.documentElement.clientHeight;
  main.style.minHeight = viewHeight - (navHeight + footerHeight) + 'px';
}

setHeight();

window.addEventListener('resize', setHeight);
