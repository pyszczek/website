FROM python:3
ENV PYTHONUNBUFFERED 1

RUN mkdir /app

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

ADD ./ /app

EXPOSE 8000

CMD python manage.py migrate && python /app/manage.py runserver 0.0.0.0:8000
