import graphene

import website_content.schema.schema
import users.schema.schema


class Query(website_content.schema.schema.Query, users.schema.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
