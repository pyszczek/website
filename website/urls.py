"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from website_content.api.viewsets import ArticleViewSet, EventViewSet, AboutUsViewSet
from users.api.viewsets import ProfileViewSet

router = routers.DefaultRouter()
router.register(r'articles', ArticleViewSet, basename='article')
router.register(r'events', EventViewSet, basename='event')
router.register(r'profiles', ProfileViewSet, basename='profile')
router.register(r'aboutus', AboutUsViewSet, basename='aboutus_api')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', obtain_jwt_token),
    url(r'^', include('website_content.urls')),
    url(r'^', include('users.urls')),
]
